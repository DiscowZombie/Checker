/*******************************************************************************
 * This file is part of Checker.
 *
 *     Checker is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Checker.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package fr.discowzombie.checker.listeners;

import java.util.HashMap;

import fr.discowzombie.checker.commands.CommandMap;
import fr.discowzombie.checker.core.Configuration;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.EventListener;
import net.dv8tion.jda.core.managers.GuildController;

public class BotListener implements EventListener {
	
	private static HashMap<Member, Integer> refunded;
	
	private final CommandMap commandmap;
	private final Configuration config;
	
	public BotListener(CommandMap commandmap, Configuration configuration) {
		this.commandmap = commandmap;
		this.config = configuration;
	}

	@Override
	public void onEvent(Event event) {
		if(event instanceof MessageReceivedEvent) onMessage((MessageReceivedEvent)event);
		if(event instanceof GuildMemberJoinEvent) onJoin((GuildMemberJoinEvent)event);
	}

	private void onJoin(GuildMemberJoinEvent event) {
		Guild guild = event.getGuild();
		Member m = event.getMember();
		
		Role validationdiscord = event.getJDA().getRoleById(config.getRoleValidation());
		new GuildController(guild).addRolesToMember(m, validationdiscord).queue();
		
		TextChannel joinedChannel = guild.getTextChannelById(config.getJoinedChannel());
		joinedChannel.sendMessage(config.getWelcomeMessage().replaceAll("%user_mention", m.getAsMention()).replaceAll("%user_name", m.getEffectiveName())).queue();
	}

	private void onMessage(MessageReceivedEvent event) {
		if(event.getAuthor().equals(event.getJDA().getSelfUser())) return;
		if(!(event.getChannel() instanceof TextChannel)) return;

		String message = event.getMessage().getContentDisplay();
		
		Role validationdiscord = event.getJDA().getRoleById(config.getRoleValidation());
		Role member = event.getJDA().getRoleById(config.getRoleMember());
		
		if(event.getMember().getRoles().contains(validationdiscord)){
			if(event.getTextChannel().getName().equalsIgnoreCase(event.getGuild().getTextChannelById(config.getJoinedChannel()).getName())){
				if(message.equalsIgnoreCase(commandmap.getTag()+"oui")){
					//Accepter
					new GuildController(event.getGuild()).removeSingleRoleFromMember(event.getMember(), validationdiscord).queue();
					new GuildController(event.getGuild()).addSingleRoleToMember(event.getMember(), member).queue();
					
					/*TextChannel generalpublic = event.getGuild().getTextChannelById(config.getGeneralChannel());
					
					BufferedImage bufferedImage = new BufferedImage(1024, 480, BufferedImage.TYPE_INT_RGB);
			        Graphics g = bufferedImage.getGraphics();

			        g.drawString("BIENVENUE", 400, 200);
			        g.drawString(event.getMember().getEffectiveName(), 420, 200);
					
			        File welcome = new File("welcome.jpg");
			        try {
						ImageIO.write(bufferedImage, "jpg", welcome);
					} catch (IOException e) {
						e.printStackTrace();
					}
			        
			        generalpublic.sendFile(welcome, null).queue();*/
					return;
				}else if(message.equalsIgnoreCase(commandmap.getTag()+"non")){
					int qtt = (refunded.get(event.getMember()) == null ? 0 : refunded.get(event.getMember()));
					qtt++;
					refunded.put(event.getMember(), qtt);
					if(qtt == 3){
						new GuildController(event.getGuild()).ban(event.getMember(), 9999, "Vous avez refuser trop de fois le règlement !").queue();
					}else{
						new GuildController(event.getGuild()).kick(event.getMember(), "Vous devez accepter le règlement pour utiliser le Discord !").queue();
					}
					return;
				}
				event.getTextChannel().sendMessage("**Erreur:** Vous devez écrire **%oui** pour accepter le règlement.").queue();
				return;
			}
		}
		
		if(message.startsWith(commandmap.getTag())){
			message = message.replaceFirst(commandmap.getTag(), "");
			commandmap.commandUser(event.getTextChannel(), event.getAuthor(), message, event.getMessage());
		}
	}

}
