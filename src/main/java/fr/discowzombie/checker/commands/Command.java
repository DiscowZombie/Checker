/*******************************************************************************
 * This file is part of Checker.
 *
 *     Checker is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Checker.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package fr.discowzombie.checker.commands;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Command {

    String name();

    String description() default "Sans description.";

    ExecutorType type() default ExecutorType.ALL;

    int power() default 0;

    enum ExecutorType {
        ALL,
        USER,
        CONSOLE
    }
}