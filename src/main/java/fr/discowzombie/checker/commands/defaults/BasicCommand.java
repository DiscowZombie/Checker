/*******************************************************************************
 * This file is part of Checker.
 *
 *     Checker is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Checker.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package fr.discowzombie.checker.commands.defaults;

import fr.discowzombie.checker.commands.CommandMap;
import fr.discowzombie.checker.commands.SimpleCommand;
import fr.discowzombie.checker.core.Checker;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

import java.awt.Color;

import fr.discowzombie.checker.commands.Command;
import fr.discowzombie.checker.commands.Command.ExecutorType;

public class BasicCommand {

	private Checker checker;
	private CommandMap commandMap;
	
	public BasicCommand(Checker checker, CommandMap commandMap) {
		this.checker = checker;
		this.commandMap = commandMap;
	}
	
	@Command(name="stop", type=ExecutorType.CONSOLE, description="Eteindre le bot")
	private void stop(){
		checker.stop();
	}
	
	@Command(name="help", type=ExecutorType.USER, description="Affiche le message d'aide")
	private void help(Guild guild, User user, MessageChannel channel){
	    
	    EmbedBuilder help = new EmbedBuilder();
	    help.setTitle("Aide: ");
	    help.setColor(Color.orange);
	    
	    for(SimpleCommand cmd : commandMap.getCommands()){
			if(cmd.getType() == ExecutorType.CONSOLE) continue;
			
			help.addField(cmd.getName(), cmd.getDescription(), false);
		}
	    
	    channel.sendMessage(help.build()).queue();
	}

}
