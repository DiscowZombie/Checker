/*******************************************************************************
 * This file is part of Checker.
 *
 *     Checker is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Checker.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package fr.discowzombie.checker.core;

import java.util.concurrent.ThreadFactory;

public class DaemonThreadFactory implements ThreadFactory {

    @Override
    public Thread newThread(Runnable r) {
        Thread result = new Thread(r);

        result.setDaemon(true);

        return result;
    }
}
