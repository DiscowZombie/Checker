/*******************************************************************************
 * This file is part of Checker.
 *
 *     Checker is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Checker.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package fr.discowzombie.checker.core;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import fr.discowzombie.checker.commands.CommandMap;
import fr.discowzombie.checker.listeners.BotListener;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Game;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class Checker {

    private static final Logger LOGGER = LoggerFactory.getLogger(Checker.class);
    
    public static Gson GSON = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
    
    private final ScheduledExecutorService executorService;
    
    private final JDA jda;

    private final Configuration configuration;
    
    private final CommandMap commandMap;

    private volatile boolean running = true;

	public Checker() throws Exception {
        LOGGER.info("Démarrage de Checker...");

        this.configuration = Configuration.load();
        
        this.executorService = Executors.newScheduledThreadPool(1, new DaemonThreadFactory());

        this.commandMap = new CommandMap(this, configuration.getCommandTag());

        jda = new JDABuilder(AccountType.BOT).setToken(configuration.getDiscordToken()).buildAsync();
        jda.addEventListener(new BotListener(commandMap, configuration));

        jda.getPresence().setGame(Game.playing("%help | Développer par DiscowZombie"));
        
        LOGGER.info("Bot démarré!");
    }

    public JDA getJda() {
        return jda;
    }

    public void loop() {
        try (Scanner scanner = new Scanner(System.in)) {
            while (running) {
                commandMap.commandConsole(scanner.nextLine());
            }
        }
    }

    public boolean isRunning() {
        return running;
    }

    public void stop() {
        this.running = false;

        this.jda.shutdown();
    }

    @Override
    protected void finalize() throws Throwable {
        this.executorService.shutdown();
    }
    
    public static Logger getLogger(){
    	return LOGGER;
    }
}