/*******************************************************************************
 * This file is part of Checker.
 *
 *     Checker is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Checker.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package fr.discowzombie.checker.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;

public class Configuration {

    public static final Path PATH = Paths.get("config.json");

    private String discordToken, commandTag, welcomeMessage;
    private Long joinedChannel, roleValidation, roleMember, generalChannel;
  
    public String getDiscordToken() {
        return discordToken;
    }
    
    public String getCommandTag(){
    	return commandTag;
    }
    
    public String getWelcomeMessage(){
    	return welcomeMessage;
    }
    
    public Long getJoinedChannel(){
    	return joinedChannel;
    }
    
    public Long getRoleValidation(){
    	return roleValidation;
    }
    
    public Long getRoleMember(){
    	return roleMember;
    }
    
    public Long getGeneralChannel(){
    	return generalChannel;
    }

    public static Configuration load() throws IOException {

        if (Files.notExists(PATH)) {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                    Configuration.class.getResourceAsStream("/fr/discowzombie/checker/config.json")
            ))) {
                Files.write(PATH, reader.lines().collect(Collectors.toList()));
            }
        }

        try (BufferedReader reader = Files.newBufferedReader(PATH, StandardCharsets.UTF_8)) {
            return Checker.GSON.fromJson(reader, Configuration.class);
        }
    }

}