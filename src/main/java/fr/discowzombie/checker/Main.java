/*******************************************************************************
 * This file is part of Checker.
 *
 *     Checker is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Checker.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package fr.discowzombie.checker;

import org.slf4j.LoggerFactory;

import fr.discowzombie.checker.core.Checker;

public final class Main {
	
	public static void main(String[] args) throws Throwable {
        try{
            new Checker().loop();
        }catch (Throwable t){
            LoggerFactory.getLogger(Checker.class).error("Impossible de démarrer le bot: ", t);
            throw t;
        }
    }

}
